package ic.swarm.stream.output


import ic.base.assert.assert
import ic.base.assert.assertDoesNotThrow
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.throwables.IoException
import ic.base.throwables.OverflowException
import ic.stream.output.ByteOutput

import ic.swarm.Swarm
import ic.swarm.block.Block
import ic.swarm.ext.writeBlock
import ic.swarm.modes.WriteMode
import ic.swarm.stream.StreamAddress
import ic.swarm.stream.impl.getLevel
import ic.swarm.stream.output.impl.DirectPacker
import ic.swarm.stream.output.impl.IndirectPacker
import ic.swarm.stream.output.impl.Packer


open class SwarmByteOutput (

	internal val swarm : Swarm,

	internal val writeMode : WriteMode

) : ByteOutput {


	open fun onClose() {}


	@Throws(IoException::class)
	private fun writeBlock (block: Block) {
		swarm.writeBlock(block, writeMode)
	}


	private var level : Int32 = 0

	private var packer : Packer = DirectPacker(writeBlock = ::writeBlock)

	private var length : Int64 = 0


	lateinit var streamAddress : StreamAddress
		private set
	;


	@Throws(IoException::class)
	override fun putByte (byte: Byte) {
		try {
			packer.putByte(byte)
		} catch (_: OverflowException) {
			val hash = packer.closeWriteAndGetHash()
			level++
			packer = IndirectPacker(
				level = level,
				firstHash = hash,
				writeBlock = ::writeBlock
			)
			assertDoesNotThrow(OverflowException::class, IoException::class) {
				packer.putByte(byte)
			}
		}
		length++
		assert(
			getMessage = {
				"length: $length, level: $level, getLevel(length): ${ getLevel(length) }"
			}
		) {
			getLevel(length) == level
		}
	}


	private var isClosed : Boolean = false

	@Throws(IoException::class)
	override fun close() {
		assert { !isClosed }
		isClosed = true
		assert(
			getMessage = {
				"length: $length, level: $level, getLevel(length): ${ getLevel(length) }"
			}
		) {
			getLevel(length) == level
		}
		val hash = packer.closeWriteAndGetHash()
		streamAddress = StreamAddress(hash, length)
		swarm.storage.recentlyWrittenBlocks.flush()
		onClose()
	}


	override fun cancel() {
		assert { !isClosed }
		isClosed = true
	}


}