package ic.swarm.stream.output.impl


import ic.base.throwables.IoException

import ic.swarm.block.Block


internal inline fun DirectPacker (

	crossinline writeBlock : (Block) -> Unit

) : DirectPacker {

	return object : DirectPacker() {

		@Throws(IoException::class)
		override fun writeBlock (block: Block) {
			writeBlock(block)
		}

	}

}