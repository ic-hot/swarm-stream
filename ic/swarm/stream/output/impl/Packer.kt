package ic.swarm.stream.output.impl


import ic.base.assert.assert
import ic.base.throwables.IoException
import ic.base.throwables.OverflowException

import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal abstract class Packer {


	@Throws(IoException::class)
	protected abstract fun writeBlock (block: Block)


	private var isClosed : Boolean = false


	@Throws(OverflowException::class, IoException::class)
	protected abstract fun implementPutByte (byte: Byte)

	@Throws(OverflowException::class, IoException::class)
	fun putByte (byte: Byte) {
		if (isClosed) throw RuntimeException()
		implementPutByte(byte)
	}


	@Throws(IoException::class)
	protected abstract fun implementGetBlock() : Block

	@Throws(IoException::class)
	fun closeWriteAndGetHash() : Hash {
		assert { !isClosed }
		isClosed = true
		val block = implementGetBlock()
		writeBlock(block)
		return Hash(of = block)
	}


}