package ic.swarm.stream.output.impl


import ic.base.assert.assert
import ic.base.primitives.int32.Int32

import ic.swarm.block.Block


internal inline fun Packer (

	level : Int32,

	crossinline writeBlock : (Block) -> Unit

) : Packer {

	assert { level >= 0 }

	if (level == 0) {

		return DirectPacker(
			writeBlock = writeBlock
		)

	} else {

		return IndirectPacker(
			level = level,
			writeBlock = writeBlock
		)

	}

}