package ic.swarm.stream.output.impl


import ic.base.assert.assert
import ic.base.arrays.bytes.ByteArray
import ic.base.arrays.ext.setRange
import ic.base.assert.assertDoesNotThrow
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.throwables.IoException
import ic.base.throwables.OverflowException
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.nonBreakableForEachIndexed

import ic.swarm.block.Block
import ic.swarm.hash.Hash
import ic.swarm.stream.impl.getCapacityInBytes
import ic.swarm.stream.impl.maxHashesCountInBlock


internal abstract class IndirectPacker (

	private val level : Int32,

	firstHash : Hash?

) : Packer() {


	init {
		assert { level > 0 }
	}


	private val childrenHashes = EditableList<Hash>().apply {
		firstHash?.let { add(it) }
	}


	private var childPacker : Packer = createChildPacker()


	private val capacity = getCapacityInBytes(level = level)

	private val childCapacity = getCapacityInBytes(level = level - 1)

	private var bytesWritten : Int64 = 0

	private var childBytesWritten : Int64 = 0

	init {
		if (firstHash != null) {
			bytesWritten += childCapacity
		}
	}


	private fun createChildPacker() = Packer(
		level = level - 1,
		writeBlock = ::writeBlock
	)


	@Throws(OverflowException::class, IoException::class)
	override fun implementPutByte (byte: Byte) {
		try {
			childPacker.putByte(byte)
			childBytesWritten++
		} catch (_: OverflowException) {
			assert { childBytesWritten == childCapacity }
			if (childrenHashes.length < maxHashesCountInBlock) {
				val childHash = childPacker.closeWriteAndGetHash()
				childrenHashes.add(childHash)
				if (childrenHashes.length < maxHashesCountInBlock) {
					childPacker = createChildPacker()
					childBytesWritten = 0
					assertDoesNotThrow(OverflowException::class, IoException::class) {
						childPacker.putByte(byte)
					}
					childBytesWritten++
				} else {
					assert { childBytesWritten == childCapacity }
					assert { bytesWritten == capacity }
					throw OverflowException
				}
			} else {
				assert { childBytesWritten == childCapacity }
				assert { bytesWritten == capacity }
				throw OverflowException
			}
		}
		bytesWritten++
		assert { childBytesWritten <= childCapacity }
		assert(
			getMessage = {
				"level: $level, " +
				"capacity: $capacity, " +
				"bytesWritten: $bytesWritten, " +
				"childrenHashes.length: ${ childrenHashes.length }, " +
				"childCapacity: $childCapacity, " +
				"childBytesWritten: $childBytesWritten"
			}
		) {
			bytesWritten <= capacity
		}
	}


	@Throws(IoException::class)
	override fun implementGetBlock() : Block {
		if (childrenHashes.length < maxHashesCountInBlock) {
			val childHash = childPacker.closeWriteAndGetHash()
			childrenHashes.add(childHash)
		}
		val bytes = ByteArray(length = Block.lengthInBytes)
		childrenHashes.nonBreakableForEachIndexed { index, hash ->
			bytes.setRange(
				startIndex = Hash.lengthInBytes * index,
				hash.asByteArray
			)
		}
		return Block(bytes = bytes)
	}


}