package ic.swarm.stream.output.impl


import ic.base.arrays.bytes.ByteArray
import ic.base.arrays.ext.length
import ic.base.primitives.int32.Int32
import ic.base.throwables.OverflowException

import ic.swarm.block.Block


internal abstract class DirectPacker : Packer() {


	private val bytes = ByteArray(length = Block.lengthInBytes)

	private var index : Int32 = 0


	@Throws(OverflowException::class)
	override fun implementPutByte (byte: Byte) {
		if (index < bytes.length) {
			bytes[index++] = byte
		} else {
			throw OverflowException
		}
	}


	override fun implementGetBlock() : Block {
		return Block(bytes = bytes)
	}


}