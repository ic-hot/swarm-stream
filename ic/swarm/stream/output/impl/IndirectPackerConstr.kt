package ic.swarm.stream.output.impl


import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException

import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal inline fun IndirectPacker (

	level : Int32,

	firstHash : Hash? = null,

	crossinline writeBlock : (Block) -> Unit

) : IndirectPacker {

	return object : IndirectPacker(
		level = level,
		firstHash = firstHash
	) {

		@Throws(IoException::class)
		override fun writeBlock (block: Block) {
			writeBlock(block)
		}

	}

}