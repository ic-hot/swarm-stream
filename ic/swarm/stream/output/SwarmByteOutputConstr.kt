package ic.swarm.stream.output


import ic.swarm.Swarm
import ic.swarm.modes.WriteMode


inline fun SwarmByteOutput (

	client : Swarm,

	writeMode : WriteMode,

	crossinline onClose : SwarmByteOutput.() -> Unit

) : SwarmByteOutput {

	return object : SwarmByteOutput(
		swarm = client,
		writeMode = writeMode
	) {

		override fun onClose() {
			onClose(this)
		}

	}

}