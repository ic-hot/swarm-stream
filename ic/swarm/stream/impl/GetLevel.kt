package ic.swarm.stream.impl


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64

import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal fun getLevel (length: Int64) : Int32 {

	var level : Int32 = 0
	var capacity = Block.lengthInBytes

	while (length > capacity) {

		level++
		capacity *= (Block.lengthInBytes / Hash.lengthInBytes)

	}

	return level

}