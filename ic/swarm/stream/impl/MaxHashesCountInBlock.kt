package ic.swarm.stream.impl


import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal val maxHashesCountInBlock = Block.lengthInBytes / Hash.lengthInBytes