package ic.swarm.stream.impl


import ic.base.loop.nonBreakableRepeat
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64

import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal fun getCapacityInBytes (level: Int32) : Int64 {

	var capacity = Block.lengthInBytes.asInt64

	nonBreakableRepeat(level) {
		capacity *= (Block.lengthInBytes / Hash.lengthInBytes)
	}

	return capacity

}