package ic.swarm.stream.funs


import ic.base.throwables.IoException
import ic.stream.output.ByteOutput
import ic.stream.output.ext.writeInt64

import ic.swarm.hash.ext.writeHash
import ic.swarm.stream.StreamAddress


@Throws(IoException::class)
fun ByteOutput.writeStreamAddress (streamAddress: StreamAddress) {

	writeHash(streamAddress.hash)

	writeInt64(streamAddress.length)

}