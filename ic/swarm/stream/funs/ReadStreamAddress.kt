package ic.swarm.stream.funs


import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.readInt64

import ic.swarm.hash.ext.readHash
import ic.swarm.stream.StreamAddress


@Throws(IoException::class)
fun ByteInput.readStreamAddress() : StreamAddress {

	val hash = readHash()

	val length = readInt64()

	return StreamAddress(
		hash = hash,
		length = length
	)

}