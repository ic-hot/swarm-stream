package ic.swarm.stream


import ic.base.primitives.int64.Int64

import ic.swarm.hash.Hash


data class StreamAddress (

	val hash : Hash,

	val length : Int64

)