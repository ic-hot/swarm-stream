package ic.swarm.stream.input.impl


import ic.base.assert.assert
import ic.base.loop.nonBreakableForRange
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asByte
import ic.base.throwables.End

import ic.swarm.block.Block


internal class DirectUnpacker (

	block : Block,

	private val bytesCount : Int32

) : Unpacker() {


	private val bytes = block.asByteArray

	init {
		assert { bytesCount <= Block.lengthInBytes }
		nonBreakableForRange(bytesCount, Block.lengthInBytes) { index ->
			assert { bytes[index] == 0.asByte }
		}
	}


	private var index : Int32 = 0


	@Throws(End::class)
	override fun implementGetNextByte() : Byte {
		if (index < bytesCount) {
			return bytes[index++]
		} else {
			throw End
		}
	}


}