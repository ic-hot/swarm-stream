package ic.swarm.stream.input.impl


import ic.base.throwables.End
import ic.base.throwables.IoException


internal abstract class Unpacker {


	@Throws(End::class, IoException::class)
	protected abstract fun implementGetNextByte() : Byte


	private var isClosed : Boolean = false

	@Throws(End::class, IoException::class)
	fun getNextByte() : Byte {
		if (isClosed) throw RuntimeException()
		try {
			return implementGetNextByte()
		} catch (_: End) {
			isClosed = true
			throw End
		}
	}


}