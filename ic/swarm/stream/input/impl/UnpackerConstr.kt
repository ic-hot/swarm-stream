package ic.swarm.stream.input.impl


import ic.base.assert.assert
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.asInt32
import ic.struct.list.List

import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal inline fun Unpacker (

	block : Block,

	level : Int32,

	bytesCount : Int64,

	crossinline getBlocks : (List<Hash>) -> List<Block>

) : Unpacker {

	assert { level >= 0 }

	if (level == 0) {

		return DirectUnpacker(
			block = block,
			bytesCount = bytesCount.asInt32
		)

	} else {

		return IndirectUnpacker(
			block = block,
			level = level,
			bytesCount = bytesCount,
			getBlocks = getBlocks
		)

	}

}