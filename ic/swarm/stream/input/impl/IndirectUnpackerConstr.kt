package ic.swarm.stream.input.impl


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.throwables.IoException
import ic.struct.list.List

import ic.swarm.block.Block
import ic.swarm.hash.Hash


internal inline fun IndirectUnpacker (

	block : Block,

	level : Int32,

	bytesCount : Int64,

	crossinline getBlocks : (List<Hash>) -> List<Block>

) : IndirectUnpacker {

	return object : IndirectUnpacker(
		block = block,
		level = level,
		expectedBytesCount = bytesCount
	) {

		@Throws(IoException::class)
		override fun getBlocks (hashes: List<Hash>) : List<Block> {
			return getBlocks(hashes)
		}

	}

}