package ic.swarm.stream.input.impl


import ic.base.arrays.bytes.ext.copy.copyRange
import ic.base.arrays.bytes.ext.get
import ic.base.arrays.bytes.ext.reduce.all
import ic.base.assert.assert
import ic.base.assert.assertDoesNotThrow
import ic.base.assert.assertNot
import ic.base.loop.allInRange
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asByte
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.asInt32
import ic.base.throwables.End
import ic.base.throwables.IoException
import ic.math.funs.divCeil
import ic.struct.list.List
import ic.struct.list.ext.length.lastIndex

import ic.swarm.block.Block
import ic.swarm.hash.Hash
import ic.swarm.stream.impl.getCapacityInBytes
import ic.swarm.stream.impl.maxHashesCountInBlock


internal abstract class IndirectUnpacker

	@Throws(IoException::class)
	constructor(
		block : Block,
		private val level : Int32,
		private val expectedBytesCount : Int64
	)

	: Unpacker()

{


	@Throws(IoException::class)
	protected abstract fun getBlocks (hashes: List<Hash>) : List<Block>


	init {
		assert { level > 0 }
	}

	private val capacity = getCapacityInBytes(level = level)

	init {
		assert { expectedBytesCount <= capacity }
	}

	private val childCapacity = getCapacityInBytes(level = level - 1)

	init {
		assert { expectedBytesCount > childCapacity }
	}


	private val childHashes : List<Hash>

	init {
		val bytes = block.asByteArray
		val childrenCount = expectedBytesCount divCeil childCapacity
		assert { childrenCount in 2..maxHashesCountInBlock }
		childHashes = List(childrenCount) { index ->
			val hashStartIndex = Hash.lengthInBytes * index
			val hashBytes = bytes.copyRange(
				from = hashStartIndex,
				to = hashStartIndex + Hash.lengthInBytes
			)
			assertNot {
				hashBytes.all { it == 0.asByte }
			}
			Hash(bytes = hashBytes)
		}
		assert {
			allInRange(
				from = Hash.lengthInBytes * childrenCount,
				to = Block.lengthInBytes
			) {
				bytes[it] == 0.asByte
			}
		}
	}


	@Suppress("LeakingThis")
	private val childBlocks = getBlocks(childHashes)


	private var nextChildIndex : Int32 = 0

	private lateinit var child : Unpacker

	init {
		createNextChild()
	}

	@Throws(IoException::class, End::class)
	private fun createNextChild() {
		val childIndex = nextChildIndex
		if (childIndex < childHashes.length) {
			child = Unpacker(
				level = level - 1,
				block = childBlocks[childIndex],
				bytesCount = getChildBytesCount(childIndex),
				getBlocks = ::getBlocks
			)
			nextChildIndex++
		} else {
			throw End
		}
	}


	private fun getChildBytesCount (childIndex: Int32) : Int64 {
		assert { childIndex < childHashes.length }
		if (childIndex == childHashes.lastIndex.asInt32) {
			if (expectedBytesCount == capacity) {
				return childCapacity
			} else {
				return expectedBytesCount % childCapacity
			}
		} else {
			return childCapacity
		}
	}


	@Throws(End::class, IoException::class)
	override fun implementGetNextByte() : Byte {
		try {
			return child.getNextByte()
		} catch (_: End) {
			createNextChild()
			assertDoesNotThrow(End::class, IoException::class) {
				return child.getNextByte()
			}
		}
	}


}