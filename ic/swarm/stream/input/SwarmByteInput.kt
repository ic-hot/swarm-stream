package ic.swarm.stream.input


import ic.base.annotations.Blocking
import ic.base.assert.assertDoesNotThrow
import ic.base.assert.assertThrows
import ic.base.primitives.int64.Int64
import ic.base.throwables.End
import ic.base.throwables.IoException
import ic.stream.input.ByteInput

import ic.swarm.Swarm
import ic.swarm.ext.readBlock
import ic.swarm.ext.readBlocks
import ic.swarm.modes.ReadMode
import ic.swarm.stream.StreamAddress
import ic.swarm.stream.impl.getLevel
import ic.swarm.stream.input.impl.Unpacker


class SwarmByteInput

	@Blocking
	@Throws(IoException::class)
	constructor(
		val client : Swarm,
		streamAddress : StreamAddress,
		internal val readMode: ReadMode
	)

	: ByteInput

{


	private var bytesLeft : Int64 = streamAddress.length

	private val unpacker = Unpacker(
		block = client.readBlock(streamAddress.hash, readMode),
		level = getLevel(streamAddress.length),
		bytesCount = streamAddress.length,
		getBlocks = { hashes ->
			client.readBlocks(hashes, readMode)
		}
	)


	@Throws(IoException::class, End::class)
	override fun getNextByteOrThrowEnd() : Byte {
		if (bytesLeft == 0L) {
			assertThrows(End::class) {
				assertDoesNotThrow(IoException::class) {
					unpacker.getNextByte()
				}
			}
			throw End
		}
		val byte = assertDoesNotThrow(End::class, IoException::class) {
			unpacker.getNextByte()
		}
		bytesLeft--
		return byte
	}


	override fun close() {}


}